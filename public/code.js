var num = 1;

var src = 0;
var tgt = 0;

var buttons = [
    'select',
    'nodes',
    'edges',
    'text',
];

var mode = 'select';

initEls = [
    { data: { label: 'top left' }, classes: 'top-left' },
    { data: { label: 'top center' }, classes: 'top-center' },
    { data: { label: 'top right' }, classes: 'top-right' },

    { data: { label: 'center left' }, classes: 'center-left' },
    { data: { label: 'center center' }, classes: 'center-center' },
    { data: { label: 'center right' }, classes: 'center-right' },

    { data: { label: 'bottom left' }, classes: 'bottom-left' },
    { data: { label: 'bottom center' }, classes: 'bottom-center' },
    { data: { label: 'bottom right' }, classes: 'bottom-right' },

    { data: { label: 'multiline manual\nfoo\nbar\nbaz' }, classes: 'multiline-manual' },
    { data: { label: 'multiline auto foo bar baz' }, classes: 'multiline-auto' },
    { data: { label: 'outline' }, classes: 'outline' },

    { data: { id: 'ar-src' } },
    { data: { id: 'ar-tgt' } },
    { data: { source: 'ar-src', target: 'ar-tgt', label: 'autorotate (move my nodes)' }, classes: 'autorotate' },
    { data: { label: 'background' }, classes: 'background' }
];

addNode = function(cy, pos) {
    newNode  = { data: { label: 'node '+num, id: 'node'+num }, classes: 'top-center', position: pos };
    num++;
    cy.add(newNode);
}

addEdge = function(cy) {
    newEdge = { data: { source: src, target: tgt }};
    cy.add(newEdge);
}

resDone = function(res) {
    return res.json()
};

mainFunc = function(style) {
    var controls = $('.controls');

    buttons.forEach(function(elt) {
	var btn = $('<button>'+ elt +'</button>');
	controls.append(btn);

	btn.on('click', function(){
	    mode = elt;
	});
    });
    
    var cy = window.cy = cytoscape({
	container: document.getElementById('cy'),

	boxSelectionEnabled: false,
	autounselectify: true,

	layout: {
            name: 'grid',
            cols: 3
	},

	style: style
    });

    //cy.add(initEls);

    cy.on('tap', function(event){
	// target holds a reference to the originator
	// of the event (core or element)
	var target = event.target;
	
	if( mode === 'nodes' ) {

	    if( target === cy ){
		addNode(cy, event.position);
	    }
	}
	
	if( mode === 'edges' ) {

	    if( target === cy ){
		src = 0;
		tgt = 0;
	    } else {
		if(src === 0) {
		    src = target.data('id');
		} else {
		    tgt = target.data('id');
		    addEdge(cy);
		    src = 0;
		}
	    }
	}

	if( mode === 'text' ) {
	    let txt = prompt("Text?");
	    
	    if( target !== cy ){
		target.data('label', txt);
	    }
	}
    });
};

fetch('cy-style.json', {mode: 'no-cors'})
  .then(resDone)
  .then(mainFunc);
